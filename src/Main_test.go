package main

import (
	"testing"
	"time"
)

func TestDate_correct(t *testing.T) {
	startDate := time.Now()
	startDate.AddDate(0, -1, 0)
	endDate := time.Now()
	if checkDate(startDate, endDate) {
		t.Fail()
	}
}

func TestDate_false(t *testing.T) {
	startDate := time.Now()
	endDate := time.Now()
	endDate.AddDate(-1, 0, 0)
	if checkDate(startDate, endDate) {
		t.Fail()
	}
}

func TestDate_future(t *testing.T) {
	startDate := time.Now()
	endDate := time.Now()
	endDate.AddDate(+1, 0, 0)
	if checkDate(startDate, endDate) {
		t.Fail()
	}
}

func TestNasaParse(t *testing.T) {
	chTest := make(chan string)
	inputData := []byte("{\"copyright\":\"Amir H. Abolfath\",\"date\":\"2019-12-06\",\"explanation\":\"This cosmic vista stretches almost 20 degrees from top to bottom," +
		"across the dusty constellation Taurus. It begins at the Pleiades and ends at the Hyades, two star clusters recognized since antiquity in Earth's night sky." +
		"At top, the compact Pleiades star cluster is about 400 light-years away. The lovely grouping of young cluster stars shine through dusty clouds that scatter blue starlight" +
		". At bottom, the V-shaped Hyades cluster looks more spread out in comparison and lies much closer, 150 light-years away. " +
		"The Hyades cluster stars seem anchored by bright Aldebaran, a red giant star with a yellowish appearance. " +
		"But Aldebaran actually lies only 65 light-years distant and just by chance along the line of sight to the Hyades cluster. " +
		"Faint and darkly obscuring dust clouds found near the edge of the Taurus Molecular Cloud are also evident throughout the celestial scene." +
		" The wide field of view includes the dark nebula Barnard 22 at left with youthful star T Tauri and Hind's variable nebula just above Aldebaran in the frame." +
		"\",\"hdurl\":\"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg\",\"media_type\":\"image\",\"service_version\":\"v1\",\"title\":\"Pleiades to Hyades\",\"url\":\"https://apod.nasa.gov/apod/image/1912/TaurusAbolfath1024.jpg\"}")
	go nasaParse(chTest, inputData)
	output := <-chTest
	if output != "https://apod.nasa.gov/apod/image/1912/TaurusAbolfath.jpg" {
		t.Fail()
	}
}

func TestIsEndDate(t *testing.T) {
	startDate := time.Now()
	endDate := time.Now()
	if isEndDate(startDate, endDate) {
		t.Fail()
	}
}

func TestIsEndDate_neg(t *testing.T) {
	startDate := time.Now()
	endDate := time.Now()
	endDate = endDate.AddDate(0, 1, 0)
	if !isEndDate(startDate, endDate) {
		t.Fail()
	}
}
